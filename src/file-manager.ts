///<reference path="../node_modules/devextreme/dist/ts/dx.all.d.ts" />
import "devextreme/dist/css/dx.common.css";
import "devextreme/dist/css/dx.light.compact.css";
import { customElement, bindable, autoinject, computedFrom, inject } from 'aurelia-framework';
import 'devextreme/dist/js/dx.all';
import RemoteFileProvider from 'devextreme/ui/file_manager/file_provider/remote';

@autoinject
@inject(Element)
@customElement('file-manager')
export class FileBrowser {
  @bindable to: any;

  constructor(public element: Element) {


  }
  attached() {
    
    const options: DevExpress.ui.dxFileManagerOptions = {
      currentPath: 'test',

      fileProvider: new RemoteFileProvider({
        endpointUrl: "http://localhost:5555/api/file-manager-db/FileManagerDBProviderApi"
      }),

      permissions: {
        create: true,
        copy: true,
        move: true,
        remove: true,
        rename: true,
        upload: true,
        download: true
      }
    }   
    
    new DevExpress.ui.dxFileManager(this.element, options);
  }
}
