# Doc-test

Prerequisites:
Node js
Git client
Aurelia Cli (Command line interface)

Aurelia:
npm install aurelia-cli -g

To run:
1. npm install
2. au run
3. It will display the url with the port, just go to that address in the browser.


For the server side I have used Your demo project for Asp.Net Core located in:
>  C:\Users\Public\Documents\DevExpress Demos 19.2\DevExtreme\ASP.NET MVC Controls\WidgetsGallery\ASP.NET Core
I just run it and copy the address into client application.

The file where I used FileManager is:
client application /src/file-manager.ts and file-manager.html


